Google BigQuery Guide
=====================

# Introduction

This repository provides source files to build the "Google BigQuery Guide".

# Maven

## `package`

To clean and rebuild the documentation, execute the command below.  Files will
be built under directory `target/classes/docs`.  Files are copied to directory
`docs`.

```bash
mvn \
    clean \
    package
```
